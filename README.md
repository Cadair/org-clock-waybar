# org-clock-waybar – Export the currently clocked-in task to be displayed on Waybar

## Installation

Put `org-clock-waybar.el` somewhere in your `load-path`, and `(require 'org-clock-waybar)`.

MELPA version may come soon.

You can set the file to be written by customizing `org-clock-waybar-filename`; it defaults to
`$XDG_CACHE_HOME/waybar-current-task.json` (`$XDG_CACHE_HOME` defaults to `$HOME/.cache` on XDG
compatible systems, like Linux.)

### Quelpa

If you only have [`quelpa`](https://github.com/quelpa/quelpa) installed:

```elisp
(quelpa
 '(org-clock-waybar
   :fetcher git
   :url "https://gitea.polonkai.eu/gergely/org-clock-waybar.git"))
```

or, if you have [`quelpa-use-package`](https://github.com/quelpa/quelpa-use-package) installed,
too:

```elisp
(quelpa-use-package org-clock-waybar
  :quelpa (org-clock-waybar
           :fetcher git
           :url "https://gitea.polonkai.eu/gergely/org-clock-waybar.git"))
```

## Waybar configuration

To add the current task to Waybar, add this snippet to your config:

```json
"custom/org": {
  "format": " {}",
  "return-type": "json",
  "restart-interval": 5,
  "exec": "cat /home/yourusername/.cache/waybar-current-task.json"
}
```

Then, add `custom/org` to `modules-left`/`modules-center`/`module-right` if your bar’s
configuration.  You can find a minimal working configuration in the [`examples`](./examples)
directory.

### Additional Configuration

You can also display an icon specific to the clocked tasks category with the `format-icons` key:

```json
"custom/org": {
    "format": "{icon} {}",
    "return-type": "json",
    "restart-interval": 1,
    "format-icons": {
      "refile": "",
      "ToDo": "",
    },
  "exec": "cat /home/yourusername/.cache/waybar-current-task.json"
}
```

If you use Emacs as a daemon (e.g. starting it as `emacs --daemon` or calling `(server-start)`),
you can change the `exec` command to invoke `emacsclient` directly.  Note that, since Emacsclient
can’t actually write stuff to the terminal, it will output an Emacs string full of backslashes
(see [this Emacs SE answer for details](https://emacs.stackexchange.com/a/28668/507)); thus, you
have to pipe the output through `jq fromjson`.

If you run emacs in this mode you can also eval commands on click, middle click or scroll.:

```json
"custom/org": {
    "format": "{icon} {}",
    "return-type": "json",
    "restart-interval": 1,
    "format-icons": {
      "refile": "",
      "ToDo": "",
    },
    "exec": "emacsclient --eval '(org-clock-waybar-ouptut-task)' | jq fromjson --unbuffered --compact-output",
    "on-click": "emacsclient --eval '(org-clock-out)'",
    "on-middle-click": "emacsclient --eval '(org-clock-in-last)'",
},

```

If you want the taskbar to show nothing or some other content if the emacs server is not running then you need to write a short bash script to catch when the emacsclient command returns a non-zero exit code. An example of this is:

```bash
#!/bin/bash

json=$(emacsclient --eval '(org-clock-waybar-ouptut-task)' 2> /dev/null)
status=$?
[ $status -eq 0 ] && echo $(echo $json | jq fromjson --unbuffered --compact-output) || echo ""
```

## Customization

To see a list of configurable parts, use `M-x customize-group <RET> org-clock-waybar`.
